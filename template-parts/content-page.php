<?php

/**
 * Template used for displaying page content in page.php
 *
 * @package macchiato
 */

?>

<!-- page-<?php the_ID(); ?> -->
<div id="page-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    /**
     * Functions hooked into macchiato_page_top
     *
     */
    do_action( 'macchiato_page_top' );

    /**
     * Functions hooked into macchiato_page
     * 
     * @see 10 macchiato_page_header
     * @see 20 macchiato_page_content
     */
    do_action( 'macchiato_page' );

    /**
     * Functions hooked in to macchiato_page_bottom
     *
     */
    do_action( 'macchiato_page_bottom' );
    ?>

</div>
<!-- /page-<?php the_ID(); ?> -->
