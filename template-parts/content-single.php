<?php

/**
 * Template used for displaying post content on single pages.
 *
 * @package macchiato
 */

?>

<!-- post-<?php the_ID(); ?> -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    /**
     * Functions hooked into macchiato_single_post_top
     *
     */
    do_action( 'macchiato_single_post_top' );

    /**
     * Functions hooked into macchiato_single_post
     * @see 10 macchiato_post_header
     * @see 20 macchiato_post_thumbnail 
     * @see 30 macchiato_post_content
     * @see 40 macchiato_post_meta
     */
    do_action( 'macchiato_single_post' );

    /**
     * Functions hooked in to macchiato_single_post_bottom
     *
     * @see 10 macchiato_display_comments 
     */
    do_action( 'macchiato_single_post_bottom' );
    ?>

</div>
<!-- /post-<?php the_ID(); ?> -->
