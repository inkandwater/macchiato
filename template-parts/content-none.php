<?php

/**
 * Template used for displaying a message that posts cannot be found.
 *
 * @package macchiato
 */

?>

<!-- no-results -->
<div class="no-results not-found">
    
    <!-- page-header -->
    <header class="page-header">
        <h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'macchiato' ); ?></h1>
    </header>
    <!-- /page-header -->

    <!-- page-content -->
    <div class="page-content">

        <?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

            <p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'macchiato' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

        <?php elseif ( is_search() ) : ?>

            <p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'macchiato' ); ?></p>
            <?php get_search_form(); ?>

        <?php else : ?>

            <p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'macchiato' ); ?></p>
            <?php get_search_form(); ?>

        <?php endif; ?>

    </div>
    <!-- /page-content -->

</div>
<!-- /no-results -->