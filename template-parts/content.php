<?php

/**
 * Template used to display post content.
 *
 * @package macchiato
 */

?>

<!-- post-<?php the_ID(); ?> -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    /**
     * Functions hooked in to macchiato_loop_post
     * 
     * @see 10 macchiato_post_header
     * @see 20 macchiato_post_thumbnail
     * @see 30 macchiato_post_content
     * @see 40 macchiato_post_meta
     */
    do_action( 'macchiato_loop_post' );
    ?>

</article>
<!-- /post-<?php the_ID(); ?> -->
