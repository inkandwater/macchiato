<?php

/**
 * The sidebar containing the main widget area.
 *
 * @package macchiato
 */

if ( ! is_active_sidebar( 'blog-sidebar' ) ) {
    return;
}
?>

<!-- widget-area -->
<div id="secondary" class="widget-area" role="complementary">
    <?php dynamic_sidebar( 'blog-sidebar' ); ?>
</div>
<!-- /widget-area -->