<?php

/**
 * The template for displaying 404 pages (not found).
 *
 * @package macchiato
 */

get_header(); ?>

    <!-- content-area -->
    <div id="primary" class="content-area">

        <!-- site-main -->
        <main id="main" class="site-main" role="main">

            <!-- error-404 -->
            <div class="error-404 not-found">

                <?php
                /**
                 * @see 10 macchiato_404_header
                 * @see 20 macchiato_404_text
                 * @see 30 macchiato_site_search
                 */
                do_action( 'macchiato_404_page_content' ); ?>

            </div><!-- /error-404 -->

        </main><!-- /site-main -->

    </div><!-- /content-area -->

<?php get_footer();