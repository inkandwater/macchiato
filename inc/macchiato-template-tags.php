<?php
/**
 * Macchiato template tags.
 *
 * @package macchiato
 */
function add_has_children_to_nav_items( $items ) {

    $parents = wp_list_pluck( $items, 'menu_item_parent');

    foreach ( $items as $item )
        in_array( $item->ID, $parents ) && $item->classes[] = 'has-sub-menu';

    return $items;

}

function macchiato_main_menu() {
    /**
     * Displays a navigation menu
     * 
     * @param array $args Arguments
     */
    $defaults = array(
        'theme_location'  => 'main-menu',
        'container'       => false,
        'menu_class'      => 'menu',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        'depth'           => 0,
        'walker'          => ''
    );
    
    wp_nav_menu( apply_filters( 'macchiato_main_menu_args', $defaults ) );

}

function macchiato_off_canvas_menu() {
    /**
     * Displays a navigation menu
     * 
     * @param array $args Arguments
     */
    $defaults = array(
        'theme_location'  => 'off-canvas-menu',
        'container'       => false,
        'menu_class'      => 'off-canvas-menu',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        'depth'           => 0,
        'walker'          => new Macchiato_Off_Canvas_Walker()
    );
    
    wp_nav_menu( apply_filters( 'macchiato_off_canvas_menu_args', $defaults ) );

}