<?php
/**
 * Macchiato hooks
 *
 * @package macchiato
 */

/**
 * General
 * 
 */
add_action( 'macchiato_sidebar',                'macchiato_get_sidebar',             10 );

/**
 * Header
 *
 * @see macchiato_header_before
 * @see macchiato_header
 * @see macchiato_content_top
 */
add_action( 'macchiato_site_before',            'macchiato_off_canvas',              10 );
add_action( 'macchiato_header',                 'macchiato_menu_toggle',             10 );
add_action( 'macchiato_header',                 'macchiato_site_branding',           20 );
add_action( 'macchiato_header',                 'macchiato_main_navigation',         40 );

/**
 * Footer
 *
 * @see macchiato_footer_before
 * @see macchiato_footer
 * @see macchiato_footer_after
 */
add_action( 'macchiato_footer_before',          'macchiato_footer_bar',              10 );
add_action( 'macchiato_footer',                 'macchiato_footer_widgets',          10 );
add_action( 'macchiato_footer',                 'macchiato_site_terms',              20 );
add_action( 'macchiato_footer_after',           'macchiato_off_canvas_close',        10 );

/**
 * Home Page
 * 
 * @see macchiato_frontpage_before
 */
add_action( 'homepage',                         'macchiato_homepage_content',        30 );
add_action( 'homepage',                         'macchiato_homepage_posts',          40 );

/**
 * Pages
 *
 * @see macchiato_page
 */
add_action( 'macchiato_page',                   'macchiato_page_header',             10 );
add_action( 'macchiato_page',                   'macchiato_page_content',            20 );

/**
 * Posts
 *
 * @see macchiato_loop_before
 * @see macchiato_loop_post
 * @see macchiato_loop_after
 */
add_action( 'macchiato_loop_post',              'macchiato_post_header',             10 );
add_action( 'macchiato_loop_post',              'macchiato_post_thumbnail',          20 );
add_action( 'macchiato_loop_post',              'macchiato_post_content',            30 );
add_action( 'macchiato_loop_post',              'macchiato_post_meta',               40 );
add_action( 'macchiato_loop_after',             'macchiato_paging_nav',              10 );
add_action( 'macchiato_single_post',            'macchiato_post_header',             10 );
add_action( 'macchiato_single_post',            'macchiato_post_thumbnail',          20 );
add_action( 'macchiato_single_post',            'macchiato_post_content',            30 );
add_action( 'macchiato_single_post',            'macchiato_post_meta',               40 );
add_action( 'macchiato_single_after',           'macchiato_post_nav',                10 );
add_action( 'macchiato_single_post_bottom',     'macchiato_display_comments',        20 );

/**
 * Search
 *
 * @see  macchiato_search_results
 */
add_action( 'macchiato_search_results_content',   'macchiato_search_results',         10 );

/**
 * 404
 *
 * @see  macchiato_404_page_content
 */
add_action( 'macchiato_404_page_content',       'macchiato_404_header',              10 );
add_action( 'macchiato_404_page_content',       'macchiato_404_text',                20 );
add_action( 'macchiato_404_page_content',       'macchiato_site_search',             30 );