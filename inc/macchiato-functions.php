<?php
/**
 * Macchiato functions.
 *
 * @package macchiato
 */

if( ! function_exists( 'is_woocommerce_activated' ) ) :
    /**
     * Is WooCommerce activated?
     * 
     * @return bool True if WooCommerce is installed and activated.
     */
    function is_woocommerce_activated() {

        return class_exists( 'woocommerce' ) ? true : false;

    }

endif;

if( ! function_exists( 'macchiato_is_product_archive' ) ) :
    /**
     * Check whether we're on a product archive page.
     *
     * @return bool Whether the current page is a product archive
     */
    function macchiato_is_product_archive() {

        if ( is_woocommerce_activated() ) {
            if ( is_shop() || is_product_taxonomy() || is_product_category() || is_product_tag() ) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

endif;

if( ! function_exists( 'macchiato_add_copyright_link' ) ) :
    /**
     * Add an extra `li` with copyright info to the footer menu
     * 
     * @param  string   $items The HTML list content for the menu items.
     * @param  stdClass $args  An object containing wp_nav_menu() arguments.
     * @return HTML     $items Amended HTML list content for the menu items.
     */
    function macchiato_add_copyright_link( $items, $args ) {

        $copyright_item = sprintf( 
            '<li><span class="copyright-text">%1s %2s</span> <a href="%3s" rel="index">%4s</a></li>',
                __( '&copy; Copyright', 'macchiato' ),
                date('Y'),
                esc_url( home_url() ),
                get_bloginfo( 'name' )
        );

        if ( $args->theme_location == 'footer-menu' ) {

            $menu_items = '';
            $menu_items .= $copyright_item . $items;

            return $menu_items;

        } else {

            return $items;

        }

    }

endif;

if ( ! function_exists( 'macchiato_site_title_or_logo' ) ) :
    /**
     * Display the site title or logo
     * 
     * @param  bool     $echo Echo the string or return it.
     * @return string   $html The HTML string containing the site title/logo.
     */
    function macchiato_site_title_or_logo( $echo = true ) {

        if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) {

            $logo = get_custom_logo();
            $html = is_home() ? '<h1 class="logo">' . $logo . '</h1>' : $logo;

        } else {

            $tag = is_home() ? 'h1' : 'h1';

            $html = '<' . esc_attr( $tag ) . ' class="site-title"><a href="' . esc_url( home_url( '/' ) ) . '" rel="home">' . esc_html( get_bloginfo( 'name' ) ) . '</a></' . esc_attr( $tag ) .'>';

            if ( '' !== get_bloginfo( 'description' ) ) {
                $html .= '<p class="site-description">' . esc_html( get_bloginfo( 'description', 'display' ) ) . '</p>';
            }

        }

        if ( ! $echo ) {
            return $html;
        }

        echo $html;
    }

endif;

if( ! function_exists( 'macchiato_do_shortcode' ) ) :
    /**
     * Call a shortcode function by tag name.
     *
     * @see  https://github.com/woocommerce/storefront/blob/master/inc/storefront-functions.php
     *
     * @param string $tag     The shortcode whose function to call.
     * @param array  $atts    The attributes to pass to the shortcode function. Optional.
     * @param array  $content The shortcode's content. Default is null (none).
     *
     * @return string|bool False on failure, the result of the shortcode on success.
     */
    function macchiato_do_shortcode( $tag, array $atts = array(), $content = null ) {
        global $shortcode_tags;

        if ( ! isset( $shortcode_tags[ $tag ] ) ) {
            return false;
        }

        return call_user_func( $shortcode_tags[ $tag ], $atts, $content, $tag );
    }

endif;