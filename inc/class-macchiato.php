<?php
/**
 * Macchiato Class
 *
 * @package macchiato
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Macchiato' ) ) :

    class Macchiato {

        public function __construct() {

            add_action( 'after_setup_theme',          array( $this, 'setup' ) );
            add_action( 'after_setup_theme',          array( $this, 'cleanup' ) );
            add_action( 'after_setup_theme',          array( $this, 'register_menus' ) );
            add_action( 'widgets_init',               array( $this, 'register_widgets' ) );
            add_action( 'wp_enqueue_scripts',         array( $this, 'styles' ) );
            add_action( 'wp_enqueue_scripts',         array( $this, 'scripts' ) );
            add_filter( 'wp_nav_menu_objects',        'add_has_children_to_nav_items',     10 );
            add_filter( 'wp_nav_menu_items',          'macchiato_add_copyright_link',      10, 2 );

        }

        /**
         * Sets up theme defaults and registers support for various WordPress features.
         *
         * Note that this function is hooked into the after_setup_theme hook, which
         * runs before the init hook. The init hook is too late for some features, such
         * as indicating support for post thumbnails.
         */
        public function setup() {

            /**
             * Set the content width in pixels, based on the theme's design and stylesheet.
             */
            $GLOBALS['content_width'] = apply_filters( 'macchiato_content_width', 1200 );

            /**
             * Load text domain
             */
            load_theme_textdomain( 'macchiato', get_template_directory() . '/languages' );

            /**
             * Let WordPress manage the document title.
             */
            add_theme_support( 'title-tag' );

            /**
             * Declare WooCommerce support.
             */
            add_theme_support( 'woocommerce' );
            add_theme_support( 'wc-product-gallery-zoom' );
            add_theme_support( 'wc-product-gallery-lightbox' );
            add_theme_support( 'wc-product-gallery-slider' );

            /**
             * Declare support for selective refreshing of widgets.
             */
            add_theme_support( 'customize-selective-refresh-widgets' );

            /**
             * Enable support for Post Thumbnails on posts and pages.
             *
             * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
             */
            add_theme_support( 'post-thumbnails', array(
                'post',
                'page'
            ) );

            add_theme_support( 'automatic-feed-links' );

            /**
             * Switch default core markup for search form, comment form, and comments
             * to output valid HTML5.
             */
            add_theme_support( 'html5', array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption'
            ) );

            /**
             * Enable support for Post Formats.
             * @see https://developer.wordpress.org/themes/functionality/post-formats/
             */
            add_theme_support( 'post-formats', array(
                'standard'
            ) );

            // Set up the WordPress core custom background feature.
            $bg_defaults = array(
                'default-color'          => '',
                'default-repeat'         => 'no-repeat',
                'default-position-x'     => '',
                'default-attachment'     => '',
                'admin-head-callback'    => '',
                'admin-preview-callback' => ''
            );

            /**
             * Set up the WordPress core custom background feature.
             */
            //add_theme_support( 'custom-background', $bg_defaults );
            
            /**
             * Set up the WordPress core custom logo feature. 
             */
            add_theme_support( 'custom-logo', array(
                'height'      => 150,
                'width'       => 300,
                'flex-height' => true,
            ) );

        }

        /**
         * Removes a bunch of default links, scripts and styles that 
         * clutter up the <head> section.
         */
        public function cleanup() {

            remove_action( 'wp_head',         'rsd_link' );
            remove_action( 'wp_head',         'wlwmanifest_link' );
            remove_action( 'wp_head',         'wp_generator' );
            remove_action( 'wp_head',         'parent_post_rel_link' );
            remove_action( 'wp_head',         'start_post_rel_link' );
            remove_action( 'wp_head',         'index_rel_link' );
            remove_action( 'wp_head',         'adjacent_posts_rel_link' );
            remove_action( 'wp_head',         'adjacent_posts_rel_link_wp_head', 10, 0 );
            remove_action( 'wp_head',         'wp_shortlink_wp_head' );
            remove_action( 'wp_head',         'feed_links',                      2 );
            remove_action( 'wp_head',         'feed_links_extra',                3 );
            remove_action( 'wp_head',         'wp_shortlink_wp_head',            10, 0 );
            remove_action( 'wp_head',         'print_emoji_detection_script',    7 );
            remove_action( 'wp_print_styles', 'print_emoji_styles' );

        }

        /**
         * Registers menu locations for the theme.
         */
        public function register_menus() {

            register_nav_menus(
                array(
                    'main-menu'         => __( 'Main Menu', 'macchiato' ),
                    'off-canvas-menu'   => __( 'Off-Canvas Menu', 'macchiato' ),
                    'site-map'          => __( 'Site Map', 'macchiato' ),
                    'footer-menu'       => __( 'Footer Menu', 'macchiato' )
                )
            );

        }

        /**
         * Registers widget areas
         *
         * Footer widget areas can be filtered to change the 
         * number (this also helps with column appearance).
         */
        public function register_widgets() {

            $footer_widget_areas = apply_filters( 'macchiato_footer_widget_areas', 4 );

            for( $i = 1; $i <= intval( $footer_widget_areas ); $i++ ) {
                register_sidebar( array(
                    "name"              => sprintf( __( "Footer Widget Area %s", 'macchiato' ), $i ),
                    "id"                => sprintf( "footer-widget-area-%s", $i ),
                    'description'       => sprintf( __( "Footer Widget Area %s", 'macchiato' ), $i ),
                    'before_widget'     => '<div id="%1$s" class="widget %2$s">',
                    'after_widget'      => '</div>',
                    'before_title'      => '<span class="widget-title">',
                    'after_title'       => '</span>',
                ) );
            }

            register_sidebar( array(
                "name"              => __( "Footer Bar", 'macchiato' ),
                "id"                => 'footer-bar',
                'description'       => __( '', 'macchiato' ),
                'before_widget'     => '<div id="%1$s" class="widget %2$s">',
                'after_widget'      => '</div>',
                'before_title'      => '<span class="widget-title">',
                'after_title'       => '</span>',
            ) );

            register_sidebar( array(
                "name"              => __( "Blog Sidebar", 'macchiato' ),
                "id"                => 'blog-sidebar',
                'description'       => __( '', 'macchiato' ),
                'before_widget'     => '<div id="%1$s" class="widget %2$s">',
                'after_widget'      => '</div>',
                'before_title'      => '<span class="widget-title">',
                'after_title'       => '</span>',
            ) );


            register_sidebar( array(
                "name"              => __( "Home Page Widget Area", 'macchiato' ),
                "id"                => 'home-sidebar',
                'description'       => __( '', 'macchiato' ),
                'before_widget'     => '<div id="%1$s" class="widget %2$s">',
                'after_widget'      => '</div>',
                'before_title'      => '<h2 class="widget-title section-title">',
                'after_title'       => '</h2>',
            ) );

        }

        /**
         * Registers and enqueues front end stylesheets
         */
        public function styles() {

            global $macchiato_version;

            wp_register_style( 
                'macchiato', 
                get_template_directory_uri() . '/style.css',
                '', 
                $macchiato_version, 
                'all' 
            );

            wp_enqueue_style( 'macchiato' );

            wp_register_style( 
                'font-awesome', 
                'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', 
                '', 
                '4.7.0', 
                'all' 
            );

            wp_enqueue_style( 'font-awesome' );

            wp_register_style( 
                'google-fonts', 
                'https://fonts.googleapis.com/css?family=Montserrat:400,700|Merriweather:400,400i,700,700i', 
                '', 
                $macchiato_version, 
                'all' 
            );

            wp_enqueue_style( 'google-fonts' );

        }

        /**
         * Registers and enqueues front end scripts
         */
        public function scripts() {

            global $macchiato_version;

            wp_register_script( 
                'macchiato-scripts', 
                get_theme_file_uri( 'assets/js/macchiato.min.js' ), 
                array( 'jquery' ),
                $macchiato_version, 
                true
            );

            wp_enqueue_script( 'macchiato-scripts' );

            if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
                wp_enqueue_script( 'comment-reply' );
            }

        }

    }

endif;

return new Macchiato();