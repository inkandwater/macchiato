<?php
/**
 * Macchiato Customizer Class
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Macchiato_Customizer' ) ) :

    /**
     * The Macchiato Customizer class
     */
    class Macchiato_Customizer {

        public function __construct() {

            add_action( 'customize_register', array( $this, 'customize_register' ), 10 );
            add_action( 'customize_preview_init', array( $this, 'customize_preview_js' ), 10 );

        }

        public function customize_register( $wp_customize ) {

            /**
             * Footer Bar
             * 
             */
            $wp_customize->add_section( 'macchiato_footer_bar', array(
                'title'                 => __( 'Footer Bar' ),
                'priority'              => 100,
                'description'           => __( "The footer bar is a widget area that shows just above the footer.<br><br> It's best suited to having a single inline widget, such as a mail sign up form or call to action link. Once you've added a widget, you can customize the footer bar's appearance below.", 'macchiato' )
            ) );

            // Text Color
            $wp_customize->add_setting( 'macchiato_footer_bar_text_color', array( 
                'default'               => '',
                'sanitize_callback'     => 'sanitize_hex_color',
                'transport'             => 'postMessage'
            ) );

            $wp_customize->add_control( 
                new WP_Customize_Color_Control( 
                    $wp_customize, 'macchiato_footer_bar_text_color', array(
                        'label'         => __( 'Text Colour', '' ),
                        'section'       => 'macchiato_footer_bar',
                        'settings'      => 'macchiato_footer_bar_text_color',
                        'priority'      => 10
            ) ) );

            // Background Color
            $wp_customize->add_setting( 'macchiato_footer_bar_bg_color', array( 
                'default'               => '',
                'sanitize_callback'     => 'sanitize_hex_color',
                'transport'             => 'postMessage'
            ) );

            $wp_customize->add_control( 
                new WP_Customize_Color_Control( 
                    $wp_customize, 'macchiato_footer_bar_bg_color', array(
                        'label'         => __( 'Background Colour', '' ),
                        'section'       => 'macchiato_footer_bar',
                        'settings'      => 'macchiato_footer_bar_bg_color',
                        'priority'      => 10
            ) ) );

            // Background Image
            $wp_customize->add_setting( 'macchiato_footer_bar_bg_image', array(
                'default'               => '',
                'sanitize_callback'     => 'esc_url_raw',
                'transport'             => 'refresh'
            ) );

            $wp_customize->add_control( 
                new WP_Customize_Upload_Control( 
                    $wp_customize, 'macchiato_footer_bar_bg_image', array(
                        'label'         => __( 'Background Image', '' ),
                        'section'       => 'macchiato_footer_bar',
                        'settings'      => 'macchiato_footer_bar_bg_image',
                        'priority'      => 10
            ) ) );

        }

        public function sanitize_multicheck( $values ) {

            $multi_values = !is_array( $values ) ? explode( ',', $values ) : $values;
            return !empty( $multi_values ) ? array_map( 'sanitize_text_field', $multi_values ) : array();

        }

        /**
         * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
         *
         * @since  1.0.0
         */
        public function customize_preview_js() {
            wp_enqueue_script( 
                'macchiato_customizer', 
                get_template_directory_uri() . '/assets/js/customizer.min.js', 
                array( 'customize-preview' ), 
                '1.1', 
                true 
            );
        }

    }

endif;

return new Macchiato_Customizer();