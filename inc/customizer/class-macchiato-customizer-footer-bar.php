<?php
/**
 * Macchiato Footer Bar Customizer Class
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Macchiato_Customizer_Footer_Bar' ) ) :

    class Macchiato_Customizer_Footer_Bar {

        public function __construct() {

            add_action( 'wp_enqueue_scripts',       array( $this, 'footer_bar' ),                       999 );
        }

        public function footer_bar() {

            $footer_bar_bg_image = get_theme_mod( 'macchiato_footer_bar_bg_image' );
            $footer_bar_bg       = get_theme_mod( 'macchiato_footer_bar_bg_color' );
            $footer_bar_text     = get_theme_mod( 'macchiato_footer_bar_text_color' );

            if( '' !== $footer_bar_bg_image ) :

                $footer_bar = '
                .footer-bar {
                    background-image: url(' . $footer_bar_bg_image . ');
                    background-position: center center;
                    background-size: cover;
                    background-repeat: no-repeat;
                    position: relative;
                }

                .footer-bar-overlay {
                    position: absolute;
                    top: 0;
                    right: 0;
                    bottom: 0;
                    left: 0;
                    background-color: ' . $footer_bar_bg . ';
                    opacity: 0.85;
                    z-index: 0;
                }

                .footer-bar .widget,
                .footer-bar .widget a {
                    color: ' . $footer_bar_text . ';
                }

                .footer-bar .widget h1,
                .footer-bar .widget h2,
                .footer-bar .widget h3,
                .footer-bar .widget h4,
                .footer-bar .widget h5,
                .footer-bar .widget h6 {
                    color: ' . $footer_bar_text . ';
                }';

            else :

                $footer_bar = '
                .footer-bar {
                    background-color: ' . $footer_bar_bg . ';
                    background-image: url(' . $footer_bar_bg_image . ');
                }

                .footer-bar .widget {
                    color: ' . $footer_bar_text . ';
                }

                .footer-bar .widget h1,
                .footer-bar .widget h2,
                .footer-bar .widget h3,
                .footer-bar .widget h4,
                .footer-bar .widget h5,
                .footer-bar .widget h6 {
                    color: ' . $footer_bar_text . ';
                }';

            endif;

            wp_add_inline_style( 'macchiato', $footer_bar );

        }

    }

endif;

return new Macchiato_Customizer_Footer_Bar();