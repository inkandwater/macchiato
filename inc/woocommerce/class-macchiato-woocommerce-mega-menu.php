<?php
/**
 * Macchiato WooCommerce Mega Menu Class
 *
 * @package  macchiato
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Macchiato_WooCommerce_Mega_Menu' ) ) :

    /**
     * Macchiato WooCommerce Mega Menu Class
     */
    class Macchiato_WooCommerce_Mega_Menu {

        /**
         * Setup class.
         *
         */
        public function __construct() {

            add_action( 'after_setup_theme',        array( $this, 'register_mega_menu_widget_areas' ),  10 );
            add_filter( 'wp_nav_menu_items',        array( $this, 'add_shop_link' ),          10, 2 );

        }


        /**
         * Add Shop Link to Main Menu
         *
         * If WooCommerce is active, add a 'shop' link to
         * navigation menus that opens up the 'shop-mega-menu'
         * menu location (if it has menu).
         * 
         */
        public function add_shop_link( $items, $args ) {

            $defaults = array(
                'main-menu'
            );

            $theme_locations = apply_filters( 'macchiato_shop_link_theme_locations', $defaults );

            if ( in_array( $args->theme_location, $theme_locations ) ) {

                $shop_page_url   = get_permalink( wc_get_page_id( 'shop' ) );
                $shop_page_title = get_the_title( wc_get_page_id( 'shop' ) );

                $class           = ( is_active_sidebar( 'mega-menu-1' ) )? apply_filters( 'macchiato_shop_menu_active_class', ' has-shop-mega-menu' ): null;

                if ( is_active_sidebar( 'mega-menu-5' ) ) {
                    $mega_menu_columns = apply_filters( 'macchiato_mega_menu_widget_areas', 5 );
                } elseif ( is_active_sidebar( 'mega-menu-4' ) ) {
                    $mega_menu_columns = apply_filters( 'macchiato_mega_menu_widget_areas', 4 );
                } elseif ( is_active_sidebar( 'mega-menu-3' ) ) {
                    $mega_menu_columns = apply_filters( 'macchiato_mega_menu_widget_areas', 3 );
                } elseif ( is_active_sidebar( 'mega-menu-2' ) ) {
                    $mega_menu_columns = apply_filters( 'macchiato_mega_menu_widget_areas', 2 );
                } elseif ( is_active_sidebar( 'mega-menu-1' ) ) {
                    $mega_menu_columns = apply_filters( 'macchiato_mega_menu_widget_areas', 1 );
                } else {
                    $mega_menu_columns = apply_filters( 'macchiato_mega_menu_widget_areas', 0 );
                }

                $shop_link = sprintf( 
                    '<li class="shop-menu-link%s">
                        <a href="%s" rel="index">%s</a>
                        <div class="mega-menu active-%d">%s</div>
                    </li>',
                    esc_attr( $class ),
                    esc_url( $shop_page_url ),
                    esc_html( $shop_page_title ),
                    intval( $mega_menu_columns ),
                    wp_kses_post( $this->shop_mega_menu() )
                );

                $menu_items = '';
                $menu_items .= $shop_link . $items;

                return $menu_items;

            } else {

                return $items;

            }

        }

        /**
        * Shop Mega Menu
        * 
        * @return $shop_mega_menu   wp_nav_menu
        */
        public function shop_mega_menu() {
        
            $mega_menu_widget_areas = apply_filters( 'macchiato_mega_menu_widget_areas', 5 );

            $mega_menu_columns = "";

            for( $i = 1; $i <= intval( $mega_menu_widget_areas ); $i++ ) {

                $sidebar_contents = "";
                ob_start();
                if( is_active_sidebar( 'mega-menu-'. $i ) ) :
                    echo '<div class="mega-menu-column">';
                    dynamic_sidebar( 'mega-menu-'. $i );
                    echo '</div>';
                endif;
                $sidebar_contents = ob_get_clean();
                $mega_menu_columns .= $sidebar_contents;

            }

            return $mega_menu_columns;

        }

        /**
         * Register Widget Areas
         * 
         * Add any WooCommerce specific widget areas.
         */
        public function register_mega_menu_widget_areas() {

            $mega_menu_widget_areas = apply_filters( 'macchiato_mega_menu_widget_areas', 5 );

            for( $i = 1; $i <= intval( $mega_menu_widget_areas ); $i++ ) {

                register_sidebar( array(
                    "name"              => sprintf( __( "Mega Menu Column %s", 'macchiato' ), $i ),
                    "id"                => sprintf( "mega-menu-%s", $i ),
                    'description'       => sprintf( __( "Mega Menu Column %s", 'macchiato' ), $i ),
                    'before_widget'     => '<div class="mega-menu-widget">',
                    'after_widget'      => '</div>',
                    'before_title'      => '<span class="widget-title">',
                    'after_title'       => '</span>',
                ) );

            }

        }

    }

endif;

return new Macchiato_WooCommerce_Mega_Menu();