<?php

if ( ! function_exists( 'macchiato_cart_link' ) ) :
    /**
     * Cart Link
     * Displayed a link to the cart including the number of items present and the cart total
     *
     * @return void
     * @since  1.0.0
     */
    function macchiato_cart_link() {

        if( is_woocommerce_activated() ) : ?>

            <a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'macchiato' ); ?>">

                <?php if( 0 !== WC()->cart->get_cart_contents_count() ) : ?>
                    <span class="count"><?php echo wp_kses_data( sprintf( __( '%d', WC()->cart->get_cart_contents_count(), 'macchiato' ), WC()->cart->get_cart_contents_count() ) );?>
                    </span>
                <?php endif; ?>

            </a>

        <?php endif;

    }

endif;

if ( ! function_exists( 'macchiato_site_actions' ) ) :
    /**
     * Site actions (e.g. search, user account, cart etc.)
     */
    function macchiato_site_actions() { 

        if( is_woocommerce_activated() ) : ?>

        <!-- site-actions -->
        <div class="site-actions">
            <ul class="site-actions-menu">
                <li class="action-search"><?php the_widget( 'WC_Widget_Product_Search' ); ?></li>
                <li class="action-account">
                    <a class="account-link" href="<?php echo get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ); ?>" title="<?php _e( 'My Account','woothemes' ); ?>">
                        <span class="screen-reader-text"><?php _e('My Account','woothemes'); ?></span>
                    </a>
                </li>
                <li class="action-cart">
                    <?php macchiato_cart_link(); ?>
                </li>
            </ul>
        </div>
        <!-- /site-actions -->

        <?php endif;

    }

endif;

if ( ! function_exists( 'macchiato_before_content' ) ) :
    /**
     * Before Content
     * Wraps all WooCommerce content in wrappers which match the theme markup
     *
     * @since   1.0.0
     * @return  void
     */
    function macchiato_before_content() {
        ?>
            <!-- /page-header -->
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

            <?php if( macchiato_is_product_archive() ) {

                $columns = apply_filters( 'macchiato_loop_columns', 3 );
                printf( '<div class="columns-%s">', esc_attr( $columns ) );

            }

    }

endif;

if ( ! function_exists( 'macchiato_after_content' ) ) :
    /**
     * After Content
     * Closes the wrapping divs
     *
     * @since   1.0.0
     * @return  void
     */
    function macchiato_after_content() {

        if( macchiato_is_product_archive() ) {
            echo '</div>';
        }
        ?>

            </main><!-- #main -->
        </div><!-- #primary -->

        <?php do_action( 'macchiato_shop_sidebar' );
    }

endif;

if ( ! function_exists( 'macchiato_sorting_wrapper' ) ) :
    /**
     * Sorting wrapper
     *
     * @since   1.4.3
     * @return  void
     */
    function macchiato_sorting_wrapper() {
        echo '<div class="macchiato-sorting">';
    }

endif;

if ( ! function_exists( 'macchiato_sorting_wrapper_close' ) ) :
    /**
     * Sorting wrapper close
     *
     * @since   1.4.3
     * @return  void
     */
    function macchiato_sorting_wrapper_close() {
        echo '</div>';
    }

endif;

if ( ! function_exists( 'macchiato_product_title_wrapper' ) ) :

    function macchiato_product_title_wrapper() {
        echo '<div class="product-title-wrapper">';
    }

endif;

if ( ! function_exists( 'macchiato_product_title_wrapper_end' ) ) :

    function macchiato_product_title_wrapper_end() {
        echo '</div>';
    }

endif;

if ( ! function_exists( 'macchiato_shop_messages' ) ) :
    /**
     * Macchiato shop messages
     *
     * @since   1.4.4
     * @uses    macchiato_do_shortcode
     */
    function macchiato_shop_messages() {
        if ( ! is_checkout() ) {
            //echo wp_kses_post( macchiato_do_shortcode( 'woocommerce_messages' ) );
        }
    }

endif;

if ( ! function_exists( 'macchiato_woocommerce_pagination' ) ) :
    /**
     * Macchiato WooCommerce Pagination
     * WooCommerce disables the product pagination inside the woocommerce_product_subcategories() function
     * but since Macchiato adds pagination before that function is excuted we need a separate function to
     * determine whether or not to display the pagination.
     *
     * @since 1.4.4
     */
    function macchiato_woocommerce_pagination() {
        if ( woocommerce_products_will_display() ) {
            woocommerce_pagination();
        }
    }

endif;

if ( ! function_exists( 'macchiato_get_shop_sidebar' ) ) :
    /**
     * Display macchiato sidebar
     *
     * @uses get_sidebar()
     * @since 1.0.0
     */
    function macchiato_get_shop_sidebar() {

        if ( ! is_active_sidebar( 'shop-sidebar' ) || is_product() ) {
            return;
        }
        ?>

        <!-- widget-area -->
        <div id="secondary" class="widget-area" role="complementary">
            <?php do_action( 'macchiato_secondary_top' ); ?>
            <?php dynamic_sidebar( 'shop-sidebar' ); ?>
            <?php do_action( 'macchiato_secondary_bottom' ); ?>
        </div>
        <!-- /widget-area -->

        <?php 

    }

endif;

if ( ! function_exists( 'macchiato_featured_products' ) ) :
    /**
     * Display Featured Products
     * Hooked into the `homepage` action in the homepage template
     *
     * @since  1.0.0
     * @param array $args the product section args.
     * @return void
     */
    function macchiato_featured_products( $args ) {

        if ( is_woocommerce_activated() ) {

            $args = apply_filters( 'macchiato_featured_products_args', array(
                'limit'   => 4,
                'columns' => 4,
                'orderby' => 'date',
                'order'   => 'desc',
                'show_title' => false,
                'title'   => __( 'Featured', 'macchiato' ),
            ) );

            echo '<section class="homepage-section homepage-product-section" aria-label="Featured Products">';

            do_action( 'macchiato_homepage_before_featured_products' );

            if( $args['show_title'] ) :
                echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';
            endif;

            do_action( 'macchiato_homepage_after_featured_products_title' );

            echo macchiato_do_shortcode( 'featured_products', array(
                'per_page' => intval( $args['limit'] ),
                'columns'  => intval( $args['columns'] ),
                'orderby'  => esc_attr( $args['orderby'] ),
                'order'    => esc_attr( $args['order'] ),
            ) );

            do_action( 'macchiato_homepage_after_featured_products' );

            echo '</section>';
        }
    }

endif;