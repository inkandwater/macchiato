<?php

/**
 * Macchiato WooCommerce Theme Template class
 *
 * @package Macchiato
 * @author  Ink & Water
 * @since   1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Macchiato_Woocommerce_Template' ) ) :

    class Macchiato_Woocommerce_Template {

        /**
         * Setup the template class
         * Add in custom functions to add into the child theme
         *
         * @since  1.0.0
         */
        public function __construct() {

            add_action( 'wp_loaded',                               array( $this, 'remove' ),                99 );

            /**
             * Macchiato WooCommerce Specific Hooks
             *
             * @package macchiato
             */
            add_action( 'macchiato_content_top',                   'woocommerce_breadcrumb',                 10 );
            add_action( 'woocommerce_before_main_content',         'macchiato_before_content',               10 );
            add_action( 'woocommerce_after_main_content',          'macchiato_after_content',                10 );
            add_action( 'macchiato_content_top',                   'macchiato_shop_messages',                15 );
            add_action( 'macchiato_header',                        'macchiato_site_actions',                 30 );

            /**
             * Shop Loop
             * 
             */
            add_action( 'woocommerce_after_shop_loop',             'macchiato_sorting_wrapper',             10 );
            add_action( 'woocommerce_after_shop_loop',             'woocommerce_catalog_ordering',          10 );
            add_action( 'woocommerce_after_shop_loop',             'woocommerce_result_count',              20 );
            add_action( 'woocommerce_after_shop_loop',             'woocommerce_pagination',                30 );
            add_action( 'woocommerce_after_shop_loop',             'macchiato_sorting_wrapper_close',       31 );

            add_action( 'woocommerce_before_shop_loop',            'macchiato_sorting_wrapper',             10 );
            add_action( 'woocommerce_before_shop_loop',            'woocommerce_catalog_ordering',          10 );
            add_action( 'woocommerce_before_shop_loop',            'woocommerce_result_count',              20 );
            add_action( 'woocommerce_before_shop_loop',            'macchiato_woocommerce_pagination',      30 );
            add_action( 'woocommerce_before_shop_loop',            'macchiato_sorting_wrapper_close',       31 );

            add_action( 'woocommerce_before_shop_loop_item_title', 'macchiato_product_title_wrapper',       10 );
            add_action( 'woocommerce_after_shop_loop_item_title',  'macchiato_product_title_wrapper_end',   10 );
            add_action( 'macchiato_shop_sidebar',                  'macchiato_get_shop_sidebar',            10 );

        }

        public function remove() {

            remove_action( 'woocommerce_before_main_content',      'woocommerce_breadcrumb',                 20, 0 );
            remove_action( 'woocommerce_before_main_content',      'woocommerce_output_content_wrapper',     10 );
            remove_action( 'woocommerce_after_main_content',       'woocommerce_output_content_wrapper_end', 10 );
            remove_action( 'woocommerce_sidebar',                  'woocommerce_get_sidebar',                10 );
            remove_action( 'woocommerce_after_shop_loop',          'woocommerce_pagination',                 10 );
            remove_action( 'woocommerce_before_shop_loop',         'woocommerce_catalog_ordering',           30 );

        }

    }

endif;

return new Macchiato_WooCommerce_Template();