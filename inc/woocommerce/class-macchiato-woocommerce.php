<?php
/**
 * Macchiato WooCommerce Class
 *
 * @package  macchiato
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Macchiato_WooCommerce' ) ) :

    /**
     * WooCommerce Integration class
     */
    class Macchiato_WooCommerce {

        /**
         * Setup class.
         *
         * @since  1.0.0
         */
        public function __construct() {

            add_filter( 'woocommerce_enqueue_styles',               '__return_empty_array' );

            add_action( 'homepage',                                     'macchiato_featured_products',            10 );
            add_action( 'macchiato_header',                             'macchiato_site_actions',                 30 );

            add_action( 'wp_enqueue_scripts',                           array( $this, 'woocommerce_scripts' ),    20 );
            add_action( 'widgets_init',                                 array( $this, 'register_widgets' ),       10 );
            add_filter( 'loop_shop_columns',                            array( $this, 'loop_columns' ),           10 );
            add_filter( 'body_class',                                   array( $this, 'woocommerce_body_class' ), 10 );
            add_filter( 'woocommerce_product_thumbnails_columns',       array( $this, 'thumbnail_columns' ),      10 );
            add_filter( 'loop_shop_per_page',                           array( $this, 'products_per_page' ),      10 );
            add_filter( 'woocommerce_output_related_products_args',     array( $this, 'related_products_args' ),  10 );
            add_filter( 'woocommerce_account_menu_items',               array( $this, 'edit_my_account_labels' ), 10 );
            add_filter( 'woocommerce_single_product_carousel_options',  array( $this, 'single_product_carousel_options' ), 10 );

        }

        /**
         * WooCommerce specific scripts & stylesheets
         *
         * @since  1.0.0
         */
        public function woocommerce_scripts() {

            wp_enqueue_style( 'macchiato-woocommerce-style', get_template_directory_uri() . '/woocommerce/woocommerce.css', '' );

        }

        public function register_widgets() {

            register_sidebar( array(
                "name"              => __( "Shop Sidebar", 'macchiato' ),
                "id"                => 'shop-sidebar',
                'description'       => __( '', 'macchiato' ),
                'before_widget'     => '<div id="%1$s" class="widget %2$s">',
                'after_widget'      => '</div>',
                'before_title'      => '<span class="widget-title">',
                'after_title'       => '</span>',
            ) );

        }

        /**
         * Add 'woocommerce-active' class to the body tag
         *
         * @param  array $classes css classes applied to the body tag.
         * @return array $classes modified to include 'woocommerce-active' class
         * @since  1.0.0
         */
        public function woocommerce_body_class( $classes ) {

            if ( is_woocommerce_activated() ) :
                $classes[] = 'woocommerce-active';
            endif;

            if( macchiato_is_product_archive() ) :
                $classes[] = 'woocommerce-product-archive';
            endif;

            if( is_active_sidebar( 'shop-sidebar' ) ) : 
                $classes[] = 'has-shop-sidebar';
            endif;

            return $classes;

        }

        /**
         * Default loop columns on product archives
         *
         * @return integer products per row
         * @since  1.0.0
         */
        public function loop_columns() {
            return apply_filters( 'macchiato_loop_columns', 3 ); // 3 products per row
        }

        /**
         * Product gallery thumnail columns
         *
         * @return integer number of columns
         * @since  1.0.0
         */
        public function thumbnail_columns() {
            return intval( apply_filters( 'macchiato_product_thumbnail_columns', 6 ) );
        }

        /**
         * Products per page
         *
         * @return integer number of products
         * @since  1.0.0
         */
        public function products_per_page() {
            return intval( apply_filters( 'macchiato_products_per_page', 20 ) );
        }

        public function related_products_args() {

            $args = apply_filters( 'macchiato_related_products_args', array(
                'posts_per_page' => 3,
                'columns'        => 3,
            ) );

            return $args;

        }

        /**
         * Edit My Account Labels
         *
         * Change labels of the links in 'My Account' - these 
         * aren't editable pages, so if we want to change any 
         * names, it has to be done here.
         * 
         * @see 
         * @param  $items
         * @return $items
         */
        public function edit_my_account_labels( $items ) {

            $items['dashboard']     = __( 'My Account', 'woocommerce' );

            return $items;

        }

        /**
         * Change carousel options on single product page
         * @param array
         * @return array
         * @since  1.1.0
         */
        public function single_product_carousel_options( $options ) {

            $options['smoothHeight'] = true;

            return $options;

        }

    }

endif;

return new Macchiato_WooCommerce();