<?php
/**
 * Dropdown Menu Walker
 *
 * @package  macchiato
 * @since    1.0.0
 * 
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Macchiato_Off_Canvas_Walker' ) ) :

    class Macchiato_Off_Canvas_Walker extends Walker_Nav_Menu {

        function start_lvl( &$output, $depth = 0, $args = Array() ) {

            $indent = str_repeat("\t", $depth);
            $output .= "\n$indent<ul class=\"sub-menu\">\n";
            $output .= sprintf( '<li class="go-back"><a class="go-back-link">%s</a></li>', esc_html( 'Back', 'macchiato' ) );

        }

    }

endif;