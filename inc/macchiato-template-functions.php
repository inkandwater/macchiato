<?php
/**
 * Macchiato template functions.
 *
 * @package macchiato
 */

if ( ! function_exists( 'macchiato_menu_toggle' ) ) : 
    /**
     * Menu Toggle button for small screens
     * 
     */
    function macchiato_menu_toggle() {

        if( has_nav_menu( 'off-canvas-menu' ) ) : ?>

        <!-- off-canvas-toggle -->
        <div class="off-canvas-toggle">
            <a id="off-canvas-toggle-button" class="off-canvas-toggle-button" aria-controls="site-navigation" aria-expanded="false">
                <span class="screen-reader-text"><?php echo esc_attr( apply_filters( 'macchiato_menu_toggle_text', __( 'Menu', 'macchiato_cart_link' ) ) ); ?></span>
            </a>
        </div>
        <!-- /off-canvas-toggle -->

        <?php endif;

    }

endif;

if ( ! function_exists( 'macchiato_site_branding' ) ) :
    /**
     * Site branding wrapper and display
     */
    function macchiato_site_branding() { 
        ?>

        <!-- site-branding -->
        <div class="site-branding">
            <?php macchiato_site_title_or_logo(); ?>
        </div>
        <!-- /site-branding -->

        <?php
    }

endif;

if ( !function_exists( 'macchiato_main_navigation' ) ) :
    /**
     * Site branding wrapper and display
     */
    function macchiato_main_navigation() {

        if( has_nav_menu( 'main-menu' ) ) : ?>

            <!-- main-navigation -->
            <nav id="main-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_html_e( 'Primary Navigation', 'macchiato' ); ?>">

                <?php macchiato_main_menu(); ?>

            </nav>
            <!-- /main-navigation -->

        <?php endif;

    }

endif;

if ( ! function_exists( 'macchiato_off_canvas' ) ) :

    function macchiato_off_canvas() {

        if( has_nav_menu( 'off-canvas-menu' ) ) : ?>

            <div class="off-canvas-wrapper">

                <!-- off-canvas-navigation -->
                <nav id="off-canvas-navigation" class="off-canvas-navigation" role="navigation" aria-label="<?php esc_html_e( 'Primary Navigation', 'macchiato' ); ?>">

                    <?php macchiato_off_canvas_menu(); ?>

                </nav>
                <div class="off-canvas-overlay"></div>
                <!-- /off-canvas-navigation -->

        <?php endif;

    }

endif;

if ( ! function_exists( 'macchiato_footer_bar' ) ) :

    function macchiato_footer_bar() {

        if( is_active_sidebar( 'footer-bar' ) ) : 
            ?>

            <!-- footer-bar -->
            <aside class="footer-bar">
                <div class="content-wrapper">
                    <?php dynamic_sidebar( 'footer-bar' ); ?>
                </div>
                <div class="footer-bar-overlay"></div>
            </aside>
            <!-- /footer-bar -->

        <?php

        endif;

    }

endif;

if ( ! function_exists( 'macchiato_site_branding_footer' ) ) : 

    function macchiato_site_branding_footer() {
        ?>

        <!-- site-branding -->
        <div class="site-branding site-branding--footer">
            <h1><?php echo get_bloginfo( 'name' ); ?></h1>
            <h2><?php echo get_bloginfo( 'description' ); ?></h2>
        </div>
        <!-- /site-branding -->

        <?php

    }

endif;

if ( ! function_exists( 'macchiato_site_terms' ) ) :

    function macchiato_site_terms() {
        ?>

        <div class="site-bottom-bar">

        <!-- site-terms -->
        <div class="site-terms">
            <?php

            $defaults = array(
                    'theme_location' => 'footer-menu',
                    'container'      => false,
                    'fallback_cb'    => false,
                    'echo'           => true,
                    'depth'          => 2,
                    'menu_class'     => 'site-copyright',
                    'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            );

            wp_nav_menu( apply_filters( 'macchiato_footer_menu_args', $defaults ) ); ?>

        </div>
        <!-- /site-terms -->

        <?php
    }

endif;

if ( ! function_exists( 'macchiato_footer_widgets' ) ) :

    function macchiato_footer_widgets() {

        if ( is_active_sidebar( 'footer-widget-area-4' ) ) {
            $widget_columns = apply_filters( 'macchiato_footer_widget_areas', 4 );
        } elseif ( is_active_sidebar( 'footer-widget-area-3' ) ) {
            $widget_columns = apply_filters( 'macchiato_footer_widget_areas', 3 );
        } elseif ( is_active_sidebar( 'footer-widget-area-2' ) ) {
            $widget_columns = apply_filters( 'macchiato_footer_widget_areas', 2 );
        } elseif ( is_active_sidebar( 'footer-widget-area-1' ) ) {
            $widget_columns = apply_filters( 'macchiato_footer_widget_areas', 1 );
        } else {
            $widget_columns = apply_filters( 'macchiato_footer_widget_areas', 0 );
        }

        if ( $widget_columns > 0 ) : ?>

            <!-- footer-widgets  -->
            <div class="footer-widget-areas active-<?php echo intval( $widget_columns ); ?>">

                <?php
                $i = 0;
                while( $i < $widget_columns ) : $i++;

                    if( is_active_sidebar( 'footer-widget-area-' . $i ) ) : ?>

                        <aside class="footer-widget-area">
                            <?php dynamic_sidebar( 'footer-widget-area-' . $i ); ?>
                        </aside>

                    <?php endif;

                endwhile; ?>

            </div>
            <!-- /footer-widgets  -->

        <?php endif;
    }

endif;

if ( ! function_exists( 'macchiato_off_canvas_close' ) ) :

    function macchiato_off_canvas_close() {

        if( has_nav_menu( 'off-canvas-menu' ) ) : ?>

            </div><!-- /off-canvas-wrapper -->

        <?php endif;

    }

endif;

if ( ! function_exists( 'macchiato_display_comments' ) ) :
    /**
     * Macchiato display comments
     *
     * @since  1.0.0
     */
    function macchiato_display_comments() {

        // If comments are open or we have at least one comment, load up the comment template.
        if ( comments_open() || '0' != get_comments_number() ) :
            comments_template();
        endif;

    }

endif;

if ( ! function_exists( 'macchiato_page_header' ) ) :

    function macchiato_page_header() {
        ?>

        <!-- entry-header -->
        <header class="entry-header">
            <?php the_post_thumbnail( 'full' ); ?>
            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </header>
        <!-- /entry-header -->

        <?php
    }

endif;

if ( ! function_exists( 'macchiato_page_content' ) ) :

    function macchiato_page_content() {
        ?>

        <!-- entry-content -->
        <div class="entry-content">
            <?php the_content(); ?>
            <?php
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'macchiato' ),
                    'after'  => '</div>',
                ) );
            ?>
        </div>
        <!-- /entry-content -->

        <?php
    }

endif;

if ( ! function_exists( 'macchiato_post_header' ) ) :
    /**
     * Display the post header with a link to the single post
     *
     * @since 1.0.0
     */
    function macchiato_post_header() {
        ?>

        <!-- entry-header -->
        <header class="entry-header">
            <?php
            if ( is_single() ) {
                macchiato_posted_on();
                the_title( '<h1 class="entry-title">', '</h1>' );
            } else {

                if ( 'post' == get_post_type() ) {
                    macchiato_posted_on();
                }

                the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
            }
            ?>
        </header>
        <!-- /entry-header -->

        <?php
    }

endif;

if ( ! function_exists( 'macchiato_post_thumbnail' ) ) :
    /**
     * Display post thumbnail
     *
     * @var $size thumbnail size. thumbnail|medium|large|full|$custom
     * @uses has_post_thumbnail()
     * @uses the_post_thumbnail
     * @param string $size the post thumbnail size.
     * @since 1.5.0
     */
    function macchiato_post_thumbnail( $size = 'full' ) {
        if ( has_post_thumbnail() ) { 

            $size = apply_filters( 'macchiato_post_thumbnail_size', $size );
            ?>
            
            <!-- entry-thumbnail -->
            <div class="entry-thumbnail">

                <?php do_action( 'macchiato_post_thumbnail_before' ); ?>

                <?php the_post_thumbnail( $size ); ?>

                <?php do_action( 'macchiato_post_thumbnail_after' ); ?>

            </div>
            <!-- /entry-thumnbail -->

        <?php }
    }

endif;

if ( ! function_exists( 'macchiato_post_content' ) ) :
    /**
     * Display the post content with a link to the single post
     *
     * @since 1.0.0
     */
    function macchiato_post_content() {
        ?>
            <!-- entry-content -->
            <div class="entry-content">
                <?php

                /**
                 * Functions hooked in to macchiato_post_content_before action.
                 *
                 * @hooked macchiato_post_thumbnail - 10
                 */
                do_action( 'macchiato_post_content_before' );

                if( is_single() ) :
                    the_content(
                        sprintf(
                            __( 'Continue reading %s', 'macchiato' ),
                            '<span class="screen-reader-text">' . get_the_title() . '</span>'
                        )
                    );
                else :
                    the_excerpt();
                endif;

                do_action( 'macchiato_post_content_after' );

                wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'macchiato' ),
                    'after'  => '</div>',
                ) );
                ?>
            </div>
            <!-- /entry-content -->
        <?php
    }

endif;

if ( ! function_exists( 'macchiato_post_meta' ) ) :
    /**
     * Display the post meta
     *
     * @since 1.0.0
     */
    function macchiato_post_meta() {
        ?>
            <!-- entry-meta -->
            <aside class="entry-meta">
                <?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search.
                /* translators: used between list items, there is a space after the comma */
                $categories_list = get_the_category_list( __( ', ', 'macchiato' ) );

                if ( $categories_list ) : ?>
                    <div class="cat-links">
                        <?php
                        echo '<div class="label">' . esc_attr( __( 'Posted in', 'macchiato' ) ) . '</div>';
                        echo wp_kses_post( $categories_list );
                        ?>
                    </div>
                <?php endif; // End if categories. ?>

                <?php
                /* translators: used between list items, there is a space after the comma */
                $tags_list = get_the_tag_list( '', __( ', ', 'macchiato' ) );

                if ( $tags_list ) : ?>
                    <div class="tags-links">
                        <?php
                        echo '<div class="label">' . esc_attr( __( 'Tagged', 'macchiato' ) ) . '</div>';
                        echo wp_kses_post( $tags_list );
                        ?>
                    </div>
                <?php endif; // End if $tags_list. ?>

            <?php endif; // End if 'post' == get_post_type(). ?>

                <?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
                    <div class="comments-link">
                        <?php echo '<div class="label">' . esc_attr( __( 'Comments', 'macchiato' ) ) . '</div>'; ?>
                        <span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'macchiato' ), __( '1 Comment', 'macchiato' ), __( '% Comments', 'macchiato' ) ); ?></span>
                    </div>
                <?php endif; ?>
            </aside>
            <!-- /entry-meta -->
        <?php
    }

endif;

if ( ! function_exists( 'macchiato_paging_nav' ) ) :
    /**
     * Display navigation to next/previous set of posts when applicable.
     */
    function macchiato_paging_nav() {
        global $wp_query;

        $args = array(
            'type'      => 'list',
            'next_text' => _x( 'Next', 'Next post', 'macchiato' ),
            'prev_text' => _x( 'Previous', 'Previous post', 'macchiato' ),
            );

        the_posts_pagination( $args );
    }

endif;

if ( ! function_exists( 'macchiato_post_nav' ) ) :
    /**
     * Display navigation to next/previous post when applicable.
     */
    function macchiato_post_nav() {

        $args = array(
            'prev_text' => __( '<span class="nav-arrow">Older</span> <span class="nav-previous-title">%title</span>', 'macchiato' ),
            'next_text' => __( '<span class="nav-arrow">Newer</span> <span class="nav-next-title">%title</span>', 'macchiato' ),
            );
        ?>

        <!-- post-navigation -->
        <?php the_post_navigation( $args ); ?>
        <!-- /post-navigation -->

        <?php

    }

endif;

if ( ! function_exists( 'macchiato_posted_on' ) ) :
    /**
     * Prints HTML with meta information for the current post-date/time and author.
     */
    function macchiato_posted_on() {
        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
        if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time> <time class="updated" datetime="%3$s">%4$s</time>';
        }

        $time_string = sprintf( $time_string,
            esc_attr( get_the_date( 'c' ) ),
            esc_html( get_the_date() ),
            esc_attr( get_the_modified_date( 'c' ) ),
            esc_html( get_the_modified_date() )
        );

        $posted_on = sprintf(
            _x( 'Posted on %s', 'post date', 'macchiato' ),
            '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
        );

        echo wp_kses( apply_filters( 'macchiato_single_post_posted_on_html', '<span class="posted-on">' . $posted_on . '</span>', $posted_on ), array(
            'span' => array(
                'class'  => array(),
            ),
            'a'    => array(
                'href'  => array(),
                'title' => array(),
                'rel'   => array(),
            ),
            'time' => array(
                'datetime' => array(),
                'class'    => array(),
            ),
        ) );
    }

endif;

if ( ! function_exists( 'macchiato_posted_by' ) ) :

    function macchiato_posted_by() {
        ?>
            <div class="author-meta">
                <?php printf( '<div class="avatar">%s</div>', get_avatar( get_the_author_meta( 'ID' ), 48 ) ); ?>
                <?php printf( '<b class="label">%s</b>', get_the_author_posts_link() ); ?>
            </div>

        <?php
    }

endif;

if ( ! function_exists( 'macchiato_get_sidebar' ) ) :
    /**
     * Display macchiato sidebar
     *
     * @uses get_sidebar()
     * @since 1.0.0
     */
    function macchiato_get_sidebar() {

        get_sidebar();

    }

endif;

if ( ! function_exists( 'macchiato_homepage_content' ) ) :

    function macchiato_homepage_content() {

        if( have_posts() ) :

            echo '<section class="homepage-section homepage-content" aria-label="Homepage Content">';

            while ( have_posts() ) : the_post();

                get_template_part( 'template-parts/content', 'page' );

            endwhile;

            wp_reset_postdata();

            echo '</section>';

        endif;

    }

endif;

if ( ! function_exists( 'macchiato_homepage_posts' ) ) :

    function macchiato_homepage_posts() {

        $defaults = array(
            'post_type'         => 'post',
            'posts_per_page'    => 5
        );

        $args = apply_filters( 'macchiato_homepage_posts_args', $defaults );

        $homepage_posts = new WP_Query( $args );

        if( $homepage_posts->have_posts() ) :

            echo '<section class="homepage-section homepage-posts" aria-label="Homepage Posts">';

            $title = sprintf( '<h2 class="section-title">%s</h2>', __( 'Posts', 'macchiato' ) );
            $title = apply_filters( 'macchiato_homepage_posts_section_title', $title );

            echo wp_kses_post( $title );

            echo '<div class="posts">';

            while ( $homepage_posts->have_posts() ) : $homepage_posts->the_post();

                get_template_part( 'template-parts/content', get_post_format() );

            endwhile;

            wp_reset_postdata();

            echo '</div>';

            if( null !== get_option( 'page_for_posts' ) ) :
                $posts_page = get_option( 'page_for_posts' );
                $classes = array( 'more-posts' );
                $classes = apply_filters( 'macchiato_homepage_posts_link_classes', $classes );
                $classes = implode( ' ', $classes );

                printf( '<a class="%1s" href="%2s">%3s</a>', esc_attr( $classes ), get_permalink( $posts_page ), __( 'View More', 'macchiato' ) );
            endif;

            echo '</section>';

        endif;

    }

endif;

if ( ! function_exists( 'macchiato_search_results' ) ) :

    function macchiato_search_results() {

        get_template_part( 'loop' );

    }

endif;

if ( ! function_exists( 'macchiato_404_header' ) ) :

    function macchiato_404_header() {
        ?>
            <!-- page-header -->
            <header class="page-header">
                <h1 class="page-title"><?php esc_html_e( '404. Page Not Found', 'macchiato' ); ?></h1>
            </header>
            <!-- /page-header -->
        <?php
    }

endif;

if ( ! function_exists( 'macchiato_404_text' ) ) :

    function macchiato_404_text() {
        ?>
            <!-- page-content -->
            <div class="page-content">
                <p><?php esc_html_e( 'There\'s no page at this location (either it\'s moved, or you typed in the URL incorrectly). To get back on track, try one of the links in the header or do a search for what you\'re after:', 'macchiato' ); ?></p>
            </div>
            <!-- /page-content -->
        <?php
    }

endif;

if ( ! function_exists( 'macchiato_site_search' ) ) :

    function macchiato_site_search() {
        ?>
            <!-- site-search -->
            <section aria-label="Search">
                <?php
                    if ( is_woocommerce_activated() ) {
                        the_widget( 'WC_Widget_Product_Search' );
                    } else {
                        get_search_form();
                    }
                ?>
            </section>
            <!-- /site-search -->
        <?php 
    }

endif;