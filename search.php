<?php

/**
 * The template for displaying search results pages.
 *
 * @package macchiato
 */
get_header(); ?>

    <!-- content-area -->
    <div id="primary" class="content-area">

        <!-- site-main -->
        <main id="main" class="site-main" role="main">

            <!-- search-results -->
            <div class="search-results">

                <?php
                /**
                 * Functions hooked into macchiato_search_results_content
                 * 
                 */
                do_action( 'macchiato_search_results_content' ); ?>

            </div><!-- /search-results -->

        </main><!-- /site-main -->

    </div><!-- /content-area -->

<?php
get_sidebar();
get_footer();