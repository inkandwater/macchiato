<?php

/**
 * Serve up a new Macchiato!
 *
 * @package macchiato
 */

/**
 * Assign the theme version to a var
 */
$theme              = wp_get_theme( 'macchiato' );
$macchiato_version  = $theme['Version'];

require 'inc/macchiato-template-tags.php';
require 'inc/macchiato-template-hooks.php';
require 'inc/macchiato-template-functions.php';
require 'inc/macchiato-functions.php';

require 'inc/customizer/class-macchiato-customizer.php';
require 'inc/customizer/class-macchiato-customizer-footer-bar.php';
require 'inc/class-macchiato-off-canvas-walker.php';
require 'inc/class-macchiato.php';

if ( is_woocommerce_activated() ) {

    require 'inc/woocommerce/class-macchiato-woocommerce.php';
    require 'inc/woocommerce/class-macchiato-woocommerce-template.php';
    require 'inc/woocommerce/class-macchiato-woocommerce-mega-menu.php';
    require 'inc/woocommerce/macchiato-woocommerce-functions.php';

}