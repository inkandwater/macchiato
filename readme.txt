=== Macchiato ===

Contributors: tomnapier, paulrmyers
Requires at least: 4.4
Tested up to: 4.6
Stable tag: 2.6.8
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

== Description ==

Integer posuere erat a ante venenatis dapibus posuere velit aliquet.

== Changelog ==

* 1.0.0 *

== Filters ==

= Shop Specific =
macchiato_loop_columns
macchiato_product_thumbnail_columns
macchiato_products_per_page
macchiato_related_products_args
macchiato_shop_link_theme_locations
macchiato_shop_menu_active_class
macchiato_shop_menu_args

= General Theme =
macchiato_content_width
macchiato_footer_widget_areas
macchiato_front_page_widget_areas
macchiato_get_small_screen_logo
macchiato_payment_types
macchiato_menu_toggle_text
macchiato_single_post_posted_on_html
macchiato_main_menu_args
macchiato_off_canvas_menu_args
macchiato_footer_menu_args

= WooCommerce Referenced in theme =
macchiato_loop_columns

== Notes for Styling ==

/**
 * Colors
 */

BG
.site-footer
.site-bottom-bar
.main-navigation li ul ul
.main-navigation > .menu > li:hover > a
.main-navigation > .menu > li.focus > a
.off-canvas-navigation
.off-canvas-menu .sub-menu
.off-canvas-overlay

.footer-bar

COLOR

.footer-bar

BORDERS

Fonts

Body Font
----
body
select
body,
button,
input,
textarea

Header Font
-------
h1,h2,h3,h4,h5,h6
.main-navigation a
.page-numbers 
.widget-area .widget-title
.mega-menu .widget-title
.woocommerce-Price-amount,
.product-title 
