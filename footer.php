<?php

/**
 * The template for displaying the footer.
 *
 * @package macchiato
 */

/**
 * Functions hooked into macchiato_content_bottom
 * 
 */
do_action( 'macchiato_content_bottom' ); ?>

</div><!-- /site-content -->

    <?php
    /**
     * Functions hooked into macchiato_footer_before
     *
     * @see 10 macchiato_footer_bar
     */
    do_action( 'macchiato_footer_before' ); ?>

    <!-- footer -->
    <footer class="site-footer">
        <div class="content-wrapper">

            <?php
            /**
             * Functions hooked into macchiato_footer
             * 
             * @see 10 macchiato_footer_widgets
             * @see 20 macchiato_site_terms
             */
            do_action( 'macchiato_footer' ); ?>

        </div>
    </footer>
    <!-- /footer -->

    <?php
    /**
     * Functions hooked into macchiato_footer_before
     *
     * @see 10 macchiato_off_canvas_close
     */
    do_action( 'macchiato_footer_after' ); ?>

</div>

<!-- scripts -->
<?php wp_footer(); ?>
<!-- /scripts -->

</body>
</html>