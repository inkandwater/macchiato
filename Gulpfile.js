'use strict';

/* --------------------------
# Dependencies
---------------------------*/

    var gulp     = require( "gulp" ), 
    sass         = require( "gulp-sass" ) ,
    autoprefixer = require( "gulp-autoprefixer" ),
    notify       = require( "gulp-notify" ),
    concat       = require( "gulp-concat" ),
    rename       = require( "gulp-rename" ),
    uglify       = require( "gulp-uglify" ),
    sourcemaps   = require( "gulp-sourcemaps" ),
    plumber      = require( "gulp-plumber" );

/* --------------------------
# Configuration
---------------------------*/

var config = {
     sassPath: './assets/sass',
     npmDir: './node_modules' ,
    jsPath: './assets/js',
}

/* --------------------------
# Sass Compile
---------------------------*/

gulp.task( 'sass', function () {
    return gulp.src( './assets/sass/**/*.scss' )
        .pipe(plumber({ errorHandler: function(err) {
            notify.onError({
                title: "Gulp error in " + err.plugin,
                message:  err.toString()
            })(err);
        }}))
        .pipe( sass({
             outputStyle: 'compressed',
            includePaths: [
                 config.sassPath,
                config.npmDir + '/bourbon/app/assets/stylesheets',
                config.npmDir + '/bourbon-neat/app/assets/stylesheets',
                config.npmDir + '/modularscale-sass/stylesheets'
             ]
         }) )
        .pipe( autoprefixer() ) 
        .pipe( gulp.dest('.') ); 
});

/* --------------------------
# Scripts Compile
---------------------------*/

gulp.task( 'scripts', function() {

    var jsScripts = [ 
        config.jsPath + '/app.js',
        config.jsPath + '/dropdown-menus.js',
        config.jsPath + '/navigation.js'
    ];

    return gulp.src( jsScripts )
        .pipe( concat( 'macchiato.min.js' ) )
        .pipe( uglify() )
        .pipe( gulp.dest( config.jsPath ) )
        .pipe( notify( { message: 'Finished minifying JavaScript'} ) );

});

/* --------------------------
# Watch Tasks
---------------------------*/

gulp.task('watch', function() {

    // Watch the input folder for change,
    // and run `sass` task when something happens
    gulp.watch( config.sassPath + '/**/*.scss', ['sass'] )

    // When there is a change,
    // log a message in the console
    .on('change', function(event) {
      console.log( 'File ' + event.path + ' was ' + event.type + ', running tasks...' );
    });

});

/* --------------------------
# Dependencies
---------------------------*/

gulp.task( 'build', [ 'sass', 'scripts' ] );
gulp.task( 'default', [ 'sass','watch' ]);
