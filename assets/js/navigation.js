/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens.
 * Also adds a focus class to parent li's for accessibility.
 * Finally adds a class required to reveal the search in the handheld 
 * footer bar.
 */
( function( $ ) {

    var menuToggle, site, body;

    // Get the thing that does the menu toggling
    menuToggle = document.getElementById( "off-canvas-toggle-button" );
    overlay = document.getElementsByClassName( "off-canvas-overlay" )[0];

    // If we can't get it, there's no point continuing.
    if ( null === menuToggle ) {
        return;
    }

    /**
     * Events
     */
    // When the menu toggler is clicked, toggle the body classes.
    menuToggle.onclick = function() {
        off_canvas_toggle();
    };

    overlay.onclick = function() {
        off_canvas_toggle('remove');
    };

    // If the user resizes the window back up to when we see the 
    // normal navigation, don't go off-canvas!
    // Note: 769 (px) is a bit of a magic number that relates to when
    // css rules within media queries...
    window.addEventListener('resize', function(){
        if( window.innerWidth > 769 ) {
            off_canvas_toggle( 'remove' );
        }
    });

    // Open Submenu on link click.
    $('.off-canvas-navigation .menu-item-has-children').children('a').on('click', function( event ){
        event.preventDefault();
        open_sub_menu( this );
    });

    // Close Submenu on go-back click
    $('.go-back').on('click', function(){
        console.log('go-back clicked');
        close_sub_menu( this );
    });

    /**
     * Functions
     */
    function off_canvas_toggle( status ) {

        // Get the body element, we'll do all the off-canvas stuff
        // just by adding and removing classes to this.
        body = document.getElementsByTagName( "body" )[0];

        if( 'remove' === status ) {
            body.className = body.className.replace( ' go-off-canvas', '' );
        } else if ( -1 !== body.className.indexOf( 'go-off-canvas' ) ) {
            body.className = body.className.replace( ' go-off-canvas', '' );
        } else {
            body.className += ' go-off-canvas';
        }

    }

    //Open submenu
    function open_sub_menu( link ) {

        var selected = $( link );
        selected.next('ul').toggleClass('is-active');

    }

    //Close submenu
    function close_sub_menu( link ) {
        var selected = $( link );
        console.log( selected.parent('ul') );
        selected.parent('ul').removeClass('is-active');

    }

} )( jQuery );