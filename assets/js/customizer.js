/**
 * Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {
    wp.customize( 'macchiato_footer_bar_bg_color', function( value ) {
        value.bind( function( to ) {
            $( '.footer-bar, .footer-bar-overlay' ).css( 'background-color', to );
        } );
    } );

    wp.customize( 'macchiato_footer_bar_text_color', function( value ) {
        value.bind( function( to ) {
            $( '.footer-bar .widget, .footer-bar .widget a' ).css( 'color', to );
        } );
    } );

    wp.customize( 'macchiato_footer_bar_text_color', function( value ) {
        value.bind( function( to ) {
            $( '.footer-bar .widget h1, .footer-bar .widget h2, .footer-bar .widget h3, .footer-bar .widget h4, .footer-bar .widget h5, .footer-bar .widget h6' ).css( 'color', to );
        } );
    } );
} )( jQuery );
