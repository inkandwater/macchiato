/**
 * app.js
 * 
 */
( function( $ ) {

    if( 'ontouchstart' in window || navigator.maxTouchPoints ) {
        document.body.classList.remove( "no-touch" );
        document.body.classList.add( "touch" );
    }

} )( jQuery );