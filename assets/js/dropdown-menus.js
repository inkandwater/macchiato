/**
 * dropdown-menus.js
 *
 * Handles toggling the navigation menu for small screens.
 *
 * The 'focus' class matches hover styles in the CSS.
 */
( function( $ ) {

  var siteNavigation = $( '#main-navigation' );

  // If there's no navigation element, bail.
  if ( ! siteNavigation.length || ! siteNavigation.children().length ) {
    return;
  }

  // Toggle 'focus' class to allow submenu access on tablets.
  function toggleFocusClassTouchScreen() {

    // Note that events are namespaced to avoid conflicts with other's JS
    $( document.body ).on( 'touchstart.macchiato', function( event ) {

      // Touch outside the menu to close it (toggling of focus class)
      if ( ! $( event.target ).closest( '.main-navigation li' ).length ) {
        $( '.main-navigation li' ).removeClass( 'focus' );
      }

    } );

    // Add the focus class to parent li's on touch
    siteNavigation.find( '.menu-item-has-children > a, .shop-menu-link > a' ).on( 'touchstart.macchiato', function( e ) {

      var el = $( this ).parent( 'li' );

      if ( ! el.hasClass( 'focus' ) ) {
        e.preventDefault();
        el.toggleClass( 'focus' );
        el.siblings( '.focus' ).removeClass( 'focus' );
      }

    });

  }

  // Run toggleFocusClassTouchScreen when we think the user has a touch screen.
  if ( is_touch_device() ) {
    $( window ).on( 'resize.macchiato', toggleFocusClassTouchScreen );
    toggleFocusClassTouchScreen();
  }

  // Add focus class on focus.
  siteNavigation.find( 'a' ).on( 'focus.macchiato blur.macchiato', function() {
    $( this ).parents( '.menu-item, .shop-menu-link' ).toggleClass( 'focus' );
  } );

  /**
   * Check if the device is touch enabled
   * [ ontouchstart for typical touch devices, maxTouchPoints check for IE/Edge ]
   */
  function is_touch_device() {
    return 'ontouchstart' in window || navigator.maxTouchPoints;
  }

} )( jQuery );