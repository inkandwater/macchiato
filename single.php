<?php

/**
 * The template for displaying all single posts.
 *
 * @package macchiato
 */

get_header(); ?>

    <!-- content-area -->
    <div id="primary" class="content-area">

        <!-- site-main -->
        <main id="main" class="site-main" role="main">

        <?php while ( have_posts() ) : the_post();

            /**
             * Functions hooked into macchiato_single_before
             * 
             */
            do_action( 'macchiato_single_before' );

            get_template_part( 'template-parts/content', 'single' );

            /**
             * Functions hooked into macchiato_single_after
             *
             * @see 10 macchiato_post_nav
             */
            do_action( 'macchiato_single_after' );

        endwhile; ?>

        </main>
        <!-- /site-main -->

    </div>
    <!-- /content-area -->

<?php
get_footer();