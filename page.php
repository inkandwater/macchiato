<?php

/**
 * The template for displaying all pages.
 *
 * @package macchiato
 */

get_header(); ?>

    <!-- content-area -->
    <div id="primary" class="content-area">

        <!-- site-main -->
        <main id="main" class="site-main" role="main">

            <?php while ( have_posts() ) : the_post();

                /**
                 * Functions hooked into macchiato_page_before
                 * 
                 */
                do_action( 'macchiato_page_before' );

                get_template_part( 'template-parts/content', 'page' );

                /**
                 * Functions hooked into macchiato_page_after
                 * 
                 */
                do_action( 'macchiato_page_after' );

            endwhile; ?>

        </main>
        <!-- /site-main -->

    </div>
    <!-- /content-area -->

<?php
get_footer();