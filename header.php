<?php

/**
 * The header for our theme.
 *
 * @package macchiato
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<!-- head -->
<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php wp_head(); ?>

</head>
<!-- /head -->

<!-- body -->
<body <?php body_class( 'no-touch' ); ?>>

    <?php 
    /**
     * Functions hooked into macchiato_site_before
     *
     * @see 10 macchiato_off_canvas
     */
    do_action( 'macchiato_site_before' ); ?>

    <div id="page" class="hfeed site">
        <?php
        /**
         * Functions hooked into macchiato_header_before
         * 
         */
        do_action( 'macchiato_header_before' ); ?>

        <!-- header -->
        <header class="site-header">
            <div class="content-wrapper">
                <?php
                /**
                 * Functions hooked into macchiato_header
                 *
                 * @see 10 macchiato_menu_toggle
                 * @see 20 macchiato_site_branding
                 * @see 40 macchiato_main_navigation
                 */
                do_action( 'macchiato_header' ); ?>
            </div>
        </header>
        <!-- /header -->

        <?php
        /**
         * Functions hooked into macchiato_content_before
         * 
         */
        do_action( 'macchiato_content_before' ); ?>

        <!-- site-content -->
        <div class="site-content">

            <?php
            /**
             * Functions hooked into macchiato_content_top
             * 
             */
            do_action( 'macchiato_content_top' );?>