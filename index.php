<?php

/**
 * The main template file.
 *
 * @package macchiato
 */

get_header(); ?>

    <!-- content-area -->
    <div id="primary" class="content-area">

        <!-- site-main -->
        <main id="main" class="site-main" role="main">

            <?php if ( have_posts() ) :

                get_template_part( 'loop' );

            else :

                get_template_part( 'template-parts/content', 'none' );

            endif; ?>

        </main>
        <!-- /site-main -->

    </div>
    <!-- /content-area -->

<?php
get_footer();